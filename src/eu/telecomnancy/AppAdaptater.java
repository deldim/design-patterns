package eu.telecomnancy;

import eu.telecomnancy.sensor.AdaptaterSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class AppAdaptater {

	public static void main(String[] args) 
	{
        ISensor sensorAdaptater = new AdaptaterSensor();
        new ConsoleUI(sensorAdaptater);
	}

}
