package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensorContext;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class AppEtat {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
        ISensor sensor =new TemperatureSensorContext();
        new ConsoleUI(sensor);
	}

}
