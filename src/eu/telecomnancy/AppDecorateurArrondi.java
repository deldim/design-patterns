package eu.telecomnancy;

import eu.telecomnancy.sensor.DecorateurArrondi;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.ui.ConsoleUI;

public class AppDecorateurArrondi {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
        ISensor sensor = new DecorateurArrondi();
        new ConsoleUI(sensor);
	}

}
