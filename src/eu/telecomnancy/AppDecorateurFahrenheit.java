package eu.telecomnancy;

import eu.telecomnancy.sensor.DecorateurFahrenheit;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.ui.ConsoleUI;

public class AppDecorateurFahrenheit {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
        ISensor sensor = new DecorateurFahrenheit();
        new ConsoleUI(sensor);

	}

}
