package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.Command;
import eu.telecomnancy.sensor.CommandValue;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.Observable;
import eu.telecomnancy.sensor.Observateur;
import eu.telecomnancy.sensor.SensorFactory;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TemperatureSensor;

import javax.swing.*;

import java.awt.*;
import java.awt.color.CMMException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;


@SuppressWarnings("serial")
public class SensorView extends JPanel implements Observateur{
    public static  ISensor sensor;
    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    public Command thecommand;
    JMenuBar menuBar = new JMenuBar();
    JMenu commande = new JMenu("Commande");
    private JMenuItem courant= new JMenuItem();
    private String name;
    

    public void actualiser(Observable o)
    {
            if(o instanceof TemperatureSensor)
            {      
                    TemperatureSensor g = (TemperatureSensor) o;
                    thecommand= new CommandeGetValue();
					this.value.setText(""+((CommandValue) thecommand).executeValue());
					repaint();
            }      
    }
    
    public SensorView(ISensor c) 
    {	
        this.sensor = c;
        this.setLayout(new BorderLayout());
        
        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              thecommand=new CommandeOn();
              thecommand.execute();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	thecommand= new CommandeOff();
                thecommand.execute();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    thecommand=new CommandeUpdate();
                    thecommand.execute();
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);
        Addproperties();
        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

    public void Addproperties()
    {
        final Properties p= new Properties();
        try {
			p.load(getClass().getResourceAsStream("/eu/telecomnancy/sensor/CommandeAutomatique.properties"));
	        for (final String i: p.stringPropertyNames()) 
	        {
	        	courant=new JMenuItem(i);
	        	courant.addActionListener(new ActionListener()
	        	{
	        		String name=i;
					@Override
					public void actionPerformed(ActionEvent arg0) 
					{
						try {
							thecommand = (Command) Class.forName(p.getProperty(name)).newInstance();
							thecommand.execute();
						} catch (InstantiationException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
	        		
	        	}
	        	);
	           // System.out.println(p.getProperty(i));
	            this.commande.add(courant);
	        }
	        
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
    }
    


}


class CommandeOn implements Command
{
	@Override
	public void execute() 
	{
		SensorView.sensor.on();
	}
	
}

class CommandeOff implements Command
{
	@Override
	public void execute() 
	{
		SensorView.sensor.off();
	}
	
}

class CommandeUpdate implements Command
{
	@Override
	public void execute() 
	{
		try {
			SensorView.sensor.update();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

class CommandeGetValue implements CommandValue
{
	@Override
	public void execute() 
	{
	}
    @Override
	public double executeValue() {
		// TODO Auto-generated method stub
		try {
			
			return SensorView.sensor.getValue();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
}