package eu.telecomnancy.ui;

import eu.telecomnancy.helpers.ReadPropertyFile;
import eu.telecomnancy.sensor.Command;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.Observateur;
import eu.telecomnancy.sensor.SensorFactory;
import eu.telecomnancy.sensor.TemperatureSensor;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;


public class MainWindow extends JFrame{

    private ISensor sensor;
    private SensorView sensorView;


    
    public MainWindow(ISensor sensor) {
        this.sensor =  sensor;
        this.sensorView = new SensorView(this.sensor);
        ((TemperatureSensor)sensor).ajouterObservateur(sensorView);
        this.setLayout(new BorderLayout());
        this.add(this.sensorView, BorderLayout.CENTER);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(300,120);
        sensorView.menuBar.add(sensorView.commande);
        //sensorView.Addproperties();
        this.setJMenuBar(sensorView.menuBar);
        
        this.setVisible(true);
    }
    
    



}


