package eu.telecomnancy.sensor;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;

public class AdaptaterSensor extends LegacyTemperatureSensor implements ISensor 
{

	@Override
	public void on() {
		if (!getStatus())
		{
			super.onOff();
		}
	}

	@Override
	public  void off() {
		if (getStatus())
		{
		 super.onOff();
		}
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		return super.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException
	{
		if (this.getStatus())
		{
		  super.onOff();
		  super.onOff();
		}
		else
		{
			throw new SensorNotActivatedException("erreur");
		}
	}

	@Override
	public double getValue() throws SensorNotActivatedException 
	{
		if (this.getStatus())
		{
		  return super.getTemperature();
		}
		else
		{
		  throw new SensorNotActivatedException("erreur");
		}
	}

}
