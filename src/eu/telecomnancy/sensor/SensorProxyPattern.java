package eu.telecomnancy.sensor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class SensorProxyPattern implements ISensor 
{
	private SimpleSensorLogger simplesensorlogger= new SimpleSensorLogger();
	SensorProxy sensorProxy= new SensorProxy(new TemperatureSensor(), simplesensorlogger);
	String format = "dd/MM/yy H:mm:ss"; 
	

	//System.out.println( formater.format( date ) ); 
	@Override
	public void on() {
		sensorProxy.on();
		java.text.SimpleDateFormat formater = new java.text.SimpleDateFormat( format );
		java.util.Date date = new java.util.Date();
		simplesensorlogger.log(LogLevel.INFO, formater.format( date ) +" ,méthode on(), état capteur : allumé");
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		sensorProxy.off();
		java.text.SimpleDateFormat formater = new java.text.SimpleDateFormat( format );
		java.util.Date date = new java.util.Date();
		simplesensorlogger.log(LogLevel.INFO, formater.format( date ) +" ,méthode off(), état capteur : éteint");

	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		java.text.SimpleDateFormat formater = new java.text.SimpleDateFormat( format );
		java.util.Date date = new java.util.Date();
		simplesensorlogger.log(LogLevel.INFO, formater.format( date ) +" ,méthode getStatus(), état capteur :"+getStatus());
		return sensorProxy.getStatus();

	}

	@Override
	public void update() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		java.text.SimpleDateFormat formater = new java.text.SimpleDateFormat( format );
		java.util.Date date = new java.util.Date();
		simplesensorlogger.log(LogLevel.INFO, formater.format( date ) +" ,méthode update(), état capteur : mis à jour");
		sensorProxy.update();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		double a =sensorProxy.getValue();
		java.text.SimpleDateFormat formater = new java.text.SimpleDateFormat( format );
		java.util.Date date = new java.util.Date();
		simplesensorlogger.log(LogLevel.INFO, formater.format( date ) +" ,méthode getValue(), valeur capteur: "+a);
		return a;
	}



}
