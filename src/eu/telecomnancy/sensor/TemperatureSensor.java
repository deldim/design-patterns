package eu.telecomnancy.sensor;

import java.util.ArrayList;
import java.util.Random;

public class TemperatureSensor implements ISensor,Observable
{
    boolean state;
    double value = 0;
    private ArrayList<Observateur> tabObservateur=new ArrayList<Observateur>();
    @Override
    public void on() {
        state = true;
    }

    @Override
    public void off() {
        state = false;
    }

    @Override
    public boolean getStatus() {
        return state;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (state)
          {
            value = (new Random()).nextDouble() * 100;
            notifierObservateurs();
          }
        	else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (state)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

	@Override
	public void ajouterObservateur(Observateur o) {
		// TODO Auto-generated method stub
		tabObservateur.add(o);
		
	}

	@Override
	public void supprimerObservateur(Observateur o) {
		// TODO Auto-generated method stub
		tabObservateur.remove(o);
	}

	@Override
	public void notifierObservateurs() {
		// TODO Auto-generated method stub
        for(int i=0;i<tabObservateur.size();i++)
        {
                Observateur o = (Observateur) tabObservateur.get(i);
                o.actualiser(this);// On utilise la méthode "tiré".
        }
	}

}
