package eu.telecomnancy.sensor;

public class TemperatureSensorContext implements ISensor
{

    ISensor sensor;
   
	@Override
	public void update() 
	{
	try {
			sensor.update();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public double getValue() 
	{
		try {
			return sensor.getValue();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public void on() 
	{
		sensor= new StateOn();
		sensor.on();
	}

	@Override
	public void off() 
	{
		sensor=new StateOff();
		sensor.off();
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		return sensor.getStatus();
	}
}
