package eu.telecomnancy.sensor;

import java.util.Random;

public class StateOn extends TemperatureSensorContext
{

    private boolean state;
    private double value = 0;
    
	@Override
	public void update()
	{
		value = (new Random()).nextDouble() * 100;
	}

	@Override
	public double getValue() {
		// TODO Auto-generated method stub
		return value;
	}

	@Override
	public void on()
	{
			this.state=true;		
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		return this.state;
	}

}
