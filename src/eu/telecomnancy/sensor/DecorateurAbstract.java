package eu.telecomnancy.sensor;

public abstract class DecorateurAbstract implements Decorateur
{

	protected ISensor temperatureSensor = new TemperatureSensor();

	public void update() throws SensorNotActivatedException
	  {
		temperatureSensor.update();
	  }

	@Override
	public void on() 
	{
		temperatureSensor.on();
	}

	@Override
	public void off() 
	{
		temperatureSensor.off();
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		return temperatureSensor.getStatus();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		return conversion();
	}

	@Override
	public abstract double conversion();
}
