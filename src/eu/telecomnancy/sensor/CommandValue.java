package eu.telecomnancy.sensor;

public interface CommandValue extends Command
{
  double executeValue();
}
